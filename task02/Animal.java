package task02;

public abstract class Animal {
    private String food;
    private String location;

    public Animal (String food, String location){
        this.food = food;
        this.location = location;
    }
    public String getFood() {
        return food;
    }


    public String getLocation() {
        return location;
    }
    public abstract void sleep();
    public abstract void makeSound();
    public abstract void eat();
}