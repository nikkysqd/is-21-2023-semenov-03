package task02;

public class task02 {
    public static void main(String[] args) {
        Animal[] animals = new Animal[3];
        animals[0] = new Cat("колбаса", "дом", "2");
        animals[1] = new Dog("мясо", "улица", "Абалдуй");
        animals[2] = new Horse("черный", "стойло", "сено");
        Veterinarian veterinarian = new Veterinarian();
        for (Animal treatAnimal : animals) {
            veterinarian.treatAnimal(treatAnimal);
        }
        Cat cat = new Cat("хлеб", "дом", "1");
        cat.eat();
        cat.sleep();
        cat.makeSound();
    }
}