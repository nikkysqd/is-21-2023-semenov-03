package task01;
public class Driver extends Person {
    private int drivingExperience;

    public Driver(String name, int drivingExperience) {
        super(name);
        this.drivingExperience = drivingExperience;
    }

    public int getDrivingExperience() {
        return drivingExperience;
    }
}
