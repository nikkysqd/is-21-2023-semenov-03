package task01;
public class Lorry extends Car {
    private int tonnage;

    public Lorry(String brand, String classCar, double weight, Driver driver, Engine engine, int tonnage) {
        super(brand, classCar, weight, driver, engine);
        this.tonnage = tonnage;
    }

    @Override
    public String toString() {
        return super.toString() + " грузоподъемность кузова " + tonnage + " т.";
    }
}