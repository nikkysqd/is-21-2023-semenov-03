package task01;
public class Engine {

    private String engineManufacturer;
    private int power;

    public Engine(int power, String engineManufacturer) {
        this.engineManufacturer = engineManufacturer;
        this.power = power;
    }
    public String getEngineManufacturer() {
        return engineManufacturer;
    }
    public int getPower() {
        return power;
    }
}