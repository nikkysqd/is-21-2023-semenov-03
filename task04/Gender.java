package task04;

enum Gender {
    MALE("мужской"),
    FEMALE("женский");

    final String gender;

    Gender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }
}

